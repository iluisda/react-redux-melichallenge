
import React from 'react';
import { Navbar, Form, InputGroup, Button, Container, Row, Col } from 'bootstrap-4-react';


const Header = () => {

    return (
     <Navbar className="bg-nav">
        <Container>
            <Row>
            <Col col="sm-2">
                <Navbar.Brand href="#">Navbar</Navbar.Brand>
            </Col>
            <Col col="sm-10">
                <InputGroup  w="100" mb="3">
                    <Form.Input type="text" placeholder="Recipient's username" />
                    <InputGroup.Append>
                        <Button outline secondary>Button</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Col>
            </Row>
        </Container>
      </Navbar>
    );
};

export default Header;